
import math 

prutok = float(input("Толщина прутка, mm: "))
yacheika = float(input("Ячейка (расстояние между серединами параллельных прутков), mm: "))
plotnost  = float(input("Плотность материала, тонн\m^3: "))

v = math.pi * (prutok / 2)**2 * 2 * (1000 / yacheika)
massa = v * plotnost / 1000
t = v / 1000

print ('Масса сетки, кг: {0}'.format(massa))
print ('Толщина листа аналогичной массы, мм: {0}'.format(t))

